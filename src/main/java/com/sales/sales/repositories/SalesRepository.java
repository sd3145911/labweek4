package com.sales.sales.repositories;

import com.sales.sales.entities.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SalesRepository extends JpaRepository<Sale, UUID> {

}
