package com.sales.sales.exceptions.sale;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class SaleInvalidException {

    @ExceptionHandler(EventAmountSale.class)
    private ResponseEntity<String> saleInvalid(){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new EventAmountSale().getMessage());
    }

    @ExceptionHandler(EventQuantityPorduct.class)
    private ResponseEntity<String> noAmount(){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new EventQuantityPorduct().getMessage());
    }

    @ExceptionHandler(EventNotProductsEnough.class)
    private ResponseEntity<String> noProductsEnough(){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new EventNotProductsEnough().getMessage());
    }

    @ExceptionHandler(EventInvalidSale.class)
    private ResponseEntity<String> saleNotDone(){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new EventInvalidSale().getMessage());
    }

}
