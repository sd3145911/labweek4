package com.sales.sales.services;

import com.sales.sales.dtos.sales.RequestSaleDTO;
import com.sales.sales.dtos.sales.ResponseSaleDTO;
import com.sales.sales.exceptions.product.EventExceptionProductNotFound;
import com.sales.sales.exceptions.sale.EventAmountSale;
import com.sales.sales.exceptions.sale.EventInvalidSale;
import com.sales.sales.exceptions.sale.EventNotProductsEnough;
import com.sales.sales.exceptions.sale.EventQuantityPorduct;
import com.sales.sales.entities.Sale;
import com.sales.sales.repositories.ProductsRepository;
import com.sales.sales.repositories.SalesRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SaleService {
    private final SalesRepository salesRepository;
    private final ProductsRepository productsRepository;

    public SaleService(ProductsRepository productsRepository, SalesRepository salesRepository) {
        this.productsRepository = productsRepository;
        this.salesRepository = salesRepository;
    }

    public ResponseSaleDTO makeSale(RequestSaleDTO requestSaleDTO){
        if(isSaleValid(requestSaleDTO)){
            var newSale = new Sale();

            BeanUtils.copyProperties(requestSaleDTO, newSale);

            newSale.setFinalValue(verifyTotalPrice(requestSaleDTO));

            salesRepository.save(newSale);

            updateQuantityProduct(requestSaleDTO);

            return new ResponseSaleDTO(newSale.getProductID(), newSale.getAmount(), newSale.getFinalValue(), newSale.getUpdatedAT());
        }

        else throw new EventInvalidSale();
    }

    public List<ResponseSaleDTO> getAllSales(){
        var sales = salesRepository.findAll();

        return sales
                .stream()
                .map(sale -> new ResponseSaleDTO(sale.getProductID(), sale.getAmount(),  sale.getFinalValue(), sale.getUpdatedAT()))
                .collect(Collectors.toList());
    }

    public boolean isSaleValid(RequestSaleDTO requestSaleDTO){
        var product = productsRepository.findById(requestSaleDTO.productID()).orElseThrow(EventExceptionProductNotFound::new);

        if(!(requestSaleDTO.amount() > 0)) throw new EventAmountSale();
        if(!(product.getQuantity() > 0)) throw new EventQuantityPorduct();
        if(!(requestSaleDTO.amount() <= product.getQuantity())) throw new EventNotProductsEnough();

        return true;
    }

    public void updateQuantityProduct(RequestSaleDTO requestSaleDTO){
        var product = productsRepository.findById(requestSaleDTO.productID()).orElseThrow(EventExceptionProductNotFound::new);

        if(product.getQuantity() > 0) product.setQuantity(product.getQuantity() - requestSaleDTO.amount());

        productsRepository.save(product);
    }

    public double verifyTotalPrice(RequestSaleDTO requestSaleDTO) {

        var product = productsRepository.findById(requestSaleDTO.productID()).orElseThrow(EventExceptionProductNotFound::new);

        double totalDiscount = 0;

        if(requestSaleDTO.amount() > 10)
            totalDiscount = 0.05;
        if(requestSaleDTO.amount() > 20)
            totalDiscount = 0.10;

        double totalPrice = requestSaleDTO.amount() * product.getPrice();

        return totalPrice - (totalPrice * totalDiscount);
    }
}
