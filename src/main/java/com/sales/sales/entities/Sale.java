package com.sales.sales.entities;

import com.sales.sales.dtos.sales.RequestSaleDTO;
import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity(name = "TB_SALES")
@Table(name = "TB_SALES")
@NoArgsConstructor
@Data
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "ID_SALE")
    private UUID saleID;
    @Column(name = "ID_PRODUCT")
    private UUID productID;
    @Column(name = "QT_AMOUNT")
    private int amount;
    @Column(name = "VL_FINAL_VALUE")
    private double finalValue;
    @Column(name = "DT_UPDATED_AT")
    private LocalDateTime updatedAT;

}
