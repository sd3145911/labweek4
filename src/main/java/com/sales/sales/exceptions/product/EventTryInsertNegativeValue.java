package com.sales.sales.exceptions.product;

public class EventTryInsertNegativeValue extends RuntimeException{
    public EventTryInsertNegativeValue(){
        super("Você não pode inserir valor negativo");
    }

}
