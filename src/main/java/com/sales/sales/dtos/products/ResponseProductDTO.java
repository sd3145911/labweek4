package com.sales.sales.dtos.products;

public record ResponseProductDTO(String name, double price, int quantity) {
}
