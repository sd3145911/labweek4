package com.sales.sales.controllers;

import com.sales.sales.dtos.sales.RequestSaleDTO;
import com.sales.sales.dtos.sales.ResponseSaleDTO;
import com.sales.sales.services.SaleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vendas")
public class SaleController {
    private final SaleService saleService;

    public SaleController(SaleService saleService) {
        this.saleService = saleService;
    }

    @PostMapping
    public ResponseEntity<ResponseSaleDTO> makeSale(@RequestBody RequestSaleDTO requestSaleDTO){
        return ResponseEntity.status(HttpStatus.CREATED).body(saleService.makeSale(requestSaleDTO));
    }

    @GetMapping
    public ResponseEntity<List<ResponseSaleDTO>> getAllSales(){
        return ResponseEntity.ok(saleService.getAllSales());
    }
}
