package com.sales.sales.exceptions.sale;

public class EventAmountSale extends RuntimeException{
    public EventAmountSale(){
        super("Você digitou uma quantidade menor ou igual a zero");
    }

}
