package com.sales.sales.dtos.products;


import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record RequestProductDTO(
        @NotNull
        String name,
        @NotBlank @Min(0)
        double price,
        @NotBlank @Min(1)
        int quantity) {
}
