package com.sales.sales.controllers;

import com.sales.sales.dtos.products.RequestProductDTO;
import com.sales.sales.dtos.products.ResponseProductDTO;
import com.sales.sales.services.ProductsServices;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("produtos")
public class ProductController {
    private final ProductsServices productsServices;

    public ProductController(ProductsServices productsServices) {
        this.productsServices = productsServices;
    }

    @GetMapping
    public ResponseEntity<List<ResponseProductDTO>> getAll(){
        return ResponseEntity.ok(productsServices.getAllProducts());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseProductDTO> getProductByID(@PathVariable UUID id){
        return ResponseEntity.ok(productsServices.getProductByID(id));
    }

    @PostMapping
    public ResponseEntity<ResponseProductDTO> postProduct(@RequestBody @Valid RequestProductDTO requestProductDTO){
        return ResponseEntity.status(HttpStatus.CREATED).body(productsServices.postProduct(requestProductDTO));
    }
}
