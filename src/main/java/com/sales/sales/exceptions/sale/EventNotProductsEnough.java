package com.sales.sales.exceptions.sale;

public class EventNotProductsEnough extends RuntimeException{
    public EventNotProductsEnough(){
        super("Você não possui produtos suficiente em estoque");
    }
}