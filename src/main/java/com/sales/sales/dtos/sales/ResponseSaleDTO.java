package com.sales.sales.dtos.sales;

import java.time.LocalDateTime;
import java.util.UUID;

public record ResponseSaleDTO(UUID productID, int amount, double finalValue, LocalDateTime updatedAt) {
}
