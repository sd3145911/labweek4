package com.sales.sales.factories;

import com.sales.sales.dtos.products.ResponseProductDTO;
import com.sales.sales.entities.Product;

import java.util.List;
import java.util.stream.Collectors;

public class ProductFactory {
    public static List<ResponseProductDTO> convertToResponseProductDTO(List<Product> products){
        return  products
                .stream()
                .map(product -> new ResponseProductDTO(product.getName(), product.getPrice(), product.getQuantity()))
                .collect(Collectors.toList());
    }
}
