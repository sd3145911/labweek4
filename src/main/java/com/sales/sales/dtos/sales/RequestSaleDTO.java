package com.sales.sales.dtos.sales;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public record RequestSaleDTO(
        @NotBlank @Min(36)
        UUID productID,
        @NotBlank @Min(1)
        int amount) {
}
