package com.sales.sales.exceptions.sale;

public class EventInvalidSale extends RuntimeException{
    public EventInvalidSale(){
        super("Venda não pode ser concluída");
    }
}