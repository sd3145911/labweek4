package com.sales.sales.services;

import com.sales.sales.dtos.products.RequestProductDTO;
import com.sales.sales.dtos.products.ResponseProductDTO;
import com.sales.sales.exceptions.product.EventExceptionProductNotFound;
import com.sales.sales.exceptions.product.EventTryInsertNegativeValue;
import com.sales.sales.entities.Product;
import com.sales.sales.factories.ProductFactory;
import com.sales.sales.repositories.ProductsRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductsServices {
    private final ProductsRepository repository;

    public ProductsServices(ProductsRepository repository) {
        this.repository = repository;
    }

    public List<ResponseProductDTO> getAllProducts(){
        return ProductFactory.convertToResponseProductDTO(repository.findAll());
    }

    public ResponseProductDTO postProduct(RequestProductDTO requestProductDTO){
        Product product = new Product();

        BeanUtils.copyProperties(requestProductDTO, product);

        if(requestProductDTO.quantity() < 0 || requestProductDTO.price() < 0)
            throw new EventTryInsertNegativeValue();

        repository.save(product);

        return new ResponseProductDTO(product.getName(), product.getPrice(), product.getQuantity());
    }

    public ResponseProductDTO getProductByID(UUID id){
        var product = repository.findById(id).orElseThrow(EventExceptionProductNotFound::new);

        return new ResponseProductDTO(product.getName(), product.getPrice(), product.getQuantity());
    }

}
