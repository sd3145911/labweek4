package com.sales.sales.exceptions.product;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ProductControllerException {

    @ExceptionHandler(EventExceptionProductNotFound.class)
    private ResponseEntity<String> productNotFound(){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new EventExceptionProductNotFound().getMessage());
    }

    @ExceptionHandler(EventTryInsertNegativeValue.class)
    private ResponseEntity<String> tryInsertNegativeValue(){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new EventTryInsertNegativeValue().getMessage());
    }
}
