package com.sales.sales.exceptions.product;

public class EventExceptionProductNotFound extends RuntimeException{
    public EventExceptionProductNotFound(){
        super("Product not found");
    }

}
