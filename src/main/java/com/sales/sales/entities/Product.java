package com.sales.sales.entities;

import com.sales.sales.dtos.products.RequestProductDTO;
import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Entity
@Table(name = "TB_PRDUCTS")
@NoArgsConstructor
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "ID_PRODUCT")
    private UUID productID;
    @Column(name = "NM_PRODUCT", unique = true)
    private String name;
    @Column(name = "VL_PRICE")
    private double price;
    @Column(name = "QT_QUANTITY")
    private int quantity;

}
